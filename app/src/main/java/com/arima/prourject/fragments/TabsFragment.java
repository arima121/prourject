package com.arima.prourject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager2.widget.ViewPager2;

import com.arima.prourject.R;
import com.arima.prourject.adapters.TabsCollectionAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class TabsFragment extends DefaultFragment {
    TabsCollectionAdapter tabsCollectionAdapter;
    ViewPager2 viewPager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public View setFragmentLayout(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.tab_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        tabsCollectionAdapter = new TabsCollectionAdapter(this);
        viewPager = view.findViewById(R.id.pager);
        viewPager.setAdapter(tabsCollectionAdapter);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        new TabLayoutMediator(
                tabLayout,
                viewPager,
                (tab, position) -> setTabIcon(tab, position)
        ).attach();
    }
    
    private void setTabIcon(TabLayout.Tab tab, int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
                tab.setText("OBJECT " + (position + 1));
                break;
            case 3:
                tab.setIcon(R.drawable.ic_new_project);
        }
    }
}

