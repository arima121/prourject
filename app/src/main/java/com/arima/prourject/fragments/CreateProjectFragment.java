package com.arima.prourject.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arima.prourject.R;
import com.arima.prourject.managers.FirebaseStorageManager;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.model.UserProject;
import com.arima.prourject.util.ImageExtras;
import com.arima.prourject.util.ScreenExtra;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Date;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class CreateProjectFragment extends DefaultFragment {
    private ImageButton selectPictureButton;
    private EditText projectText;
    private ImageView backgroundPicture;
    private FloatingActionButton createProjectButton;

    private Uri imageUri = null;

    private boolean hasImage = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        return root;
    }

    @Override
    public View setFragmentLayout(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_create_project, container, false);
    }

    @Override
    public void createViewItems(View root){
        super.createViewItems(root);
        selectPictureButton = root.findViewById(R.id.select_picture);
        backgroundPicture = root.findViewById(R.id.background_picture);
        createProjectButton = root.findViewById(R.id.create_project_button);
        projectText = root.findViewById(R.id.project_name_edit);

        createProjectButton.setOnClickListener(view -> createProject(createProjectButton));
        selectPictureButton.setOnClickListener(view -> openGalleryFragment(view));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
    }

    public void openGalleryFragment(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        imageUri = data.getData();
                        if (imageUri != null) {
                            int width = ScreenExtra.width(thisFragment.getContext());
                            int height = (int)(420 * ScreenExtra.density(thisFragment.getContext()));
                            Picasso.get()
                                    .load(imageUri)
                                    .resize(width, height)
                                    .centerCrop()
                                    .into(backgroundPicture);
                            hasImage = true;
                        }
                    }
                    break;
            }
        }
    }

    public void createProject(View view) {
        String projectName = projectText.getText().toString();
        projectName = !projectName.isEmpty() ? projectName : getString(R.string.create_project_name);
        UserProject userProject = new UserProject();
        userProject.setActive(true);
        userProject.setName(projectName);
        userProject.setCreationDate(new Timestamp(new Date()));

        setLoadingState(true);
        firestoreManager.addDocument(
                FirestoreManager.getUserProjectsPath(authManager.getUser().getId()),
                userProject,
                success -> {
                    String id = success.getId();
                    if(!hasImage) {
                        Snackbar.make(view, R.string.create_project_created, Snackbar.LENGTH_LONG).show();
                        cleanPage();
                        setLoadingState(false);
                        return;
                    }
                    try {
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(thisFragment.getContext().getContentResolver(), imageUri);
                        selectedImage = ImageExtras.scaleDown(selectedImage, 1620, true);
                        cacheManager.saveImageInCache(selectedImage, FirebaseStorageManager.PROJECT_MAIN_PIC_NAME, 70);
                        storageManager.uploadFile(
                                new File(cacheManager.getCachePath(), FirebaseStorageManager.PROJECT_MAIN_PIC_NAME), FirebaseStorageManager.getProjectMainPicPath(authManager.getUser().getId(), id),
                                uploadTask -> {
                                    if(!uploadTask.isSuccessful()) {
                                        Snackbar.make(view, R.string.create_project_error_no_pic_saved, Snackbar.LENGTH_LONG).show();
                                        cleanPage();
                                        setLoadingState(false);
                                        return;
                                    }
                                    Snackbar.make(view, R.string.create_project_created, Snackbar.LENGTH_LONG).show();
                                    cleanPage();
                                    setLoadingState(false);
                                });
                    } catch (Exception e) {
                        Snackbar.make(view, R.string.create_project_error_no_pic_saved, Snackbar.LENGTH_LONG).show();
                        cleanPage();
                        setLoadingState(false);
                    }
                },
                error -> {
                    Snackbar.make(view, R.string.create_project_error, Snackbar.LENGTH_LONG).show();
                    setLoadingState(false);
                });
    }

    public void cleanPage() {
        hasImage = false;
        projectText.setText(R.string.create_project_name);
        backgroundPicture.setImageResource(R.drawable.gradient_background);
    }
}