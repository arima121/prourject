package com.arima.prourject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.arima.prourject.R;
import com.arima.prourject.managers.CacheManager;
import com.arima.prourject.managers.FirebaseAuthManager;
import com.arima.prourject.managers.FirebaseStorageManager;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.managers.SharedPreferencesManager;

public abstract class DefaultFragment extends Fragment {
    protected DefaultFragment thisFragment;

    protected FirebaseAuthManager authManager;
    protected FirestoreManager firestoreManager;
    protected SharedPreferencesManager preferencesManager;
    protected FirebaseStorageManager storageManager;
    protected CacheManager cacheManager;

    protected ProgressBar loadingPB;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = setFragmentLayout(inflater, container);

        createInternalItems();
        createViewItems(root);

        return root;
    }

    public void createInternalItems() {
        thisFragment = this;

        authManager = new FirebaseAuthManager();
        firestoreManager = new FirestoreManager();
        storageManager = new FirebaseStorageManager();
        preferencesManager = new SharedPreferencesManager(getActivity());
        cacheManager = new CacheManager(getActivity());
    }

    public void createViewItems(View root) {
        loadingPB = root.findViewById(R.id.loading);
    }

    public abstract View setFragmentLayout(LayoutInflater inflater, ViewGroup container);

    public void setLoadingState(boolean loading) {
        if(loading) {
            thisFragment.getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            thisFragment.getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        loadingPB.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
