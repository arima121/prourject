package com.arima.prourject.util;

import android.app.Activity;
import android.content.Intent;

public class ActivityExtra {
    public static void redirectTo(Activity originActivity, Class destinyClass, boolean closeCurrent) {
        Intent intent = new Intent(originActivity, destinyClass);
        originActivity.startActivity(intent);
        if(closeCurrent) {
            originActivity.finish();
        }
    }
}
