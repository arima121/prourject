package com.arima.prourject.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

public class GsonExtras {
    public static Map<String, Object> noNUllMap(Object object) {
        Gson gson = new GsonBuilder().create();

        Map<String, Object> map = new Gson().fromJson(
                gson.toJson(object), new TypeToken<HashMap<String, Object>>() {
                }.getType()
        );

        return map;
    }
}
