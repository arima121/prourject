package com.arima.prourject.util;

import android.content.Context;

public class ScreenExtra {
    public static int height(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int width(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static float density(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }
}
