package com.arima.prourject.managers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class CacheManager {
    Activity activity;

    public CacheManager(Activity activity) {
        this.activity = activity;
    }

    public String getCachePath() {
        return activity.getCacheDir().getPath();
    }

    public boolean saveImageInCache(Bitmap bitmapToSave, String fileName, int quality) {
        String path = activity.getCacheDir().getPath();
        OutputStream fOut;
        File file = new File(path, fileName);
        try {
            fOut = new FileOutputStream(file);
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, quality, fOut);
            fOut.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }



    public Drawable readImageFromCache(String fileName) {
        return Drawable.createFromPath(getCachePath() + "/" + fileName);
    }

    public boolean exists(String fileName) {
        File file = new File(activity.getCacheDir().getPath(), fileName);
        return file.exists();
    }
}
