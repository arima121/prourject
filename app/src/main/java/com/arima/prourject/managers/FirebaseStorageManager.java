package com.arima.prourject.managers;

import android.net.Uri;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

public class FirebaseStorageManager {
    private FirebaseStorage storage;
    private StorageReference storageReference;

    public static final String IMAGE_PATH = "images";
    public static final String USER_PATH = "user";
    public static final String PROJECTS_PATH = "projects";

    public static final String PROFILE_PIC_NAME = "profile_pic.jpg";
    public static final String PROJECT_MAIN_PIC_NAME = "temp_project_main_pic.jpg";

    public FirebaseStorageManager() {
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    public void uploadFile(File cacheFile, String targetPath, OnCompleteListener<Uri> task) {
        Uri file = Uri.fromFile(cacheFile);
        StorageReference outputTask = storageReference.child(targetPath);
        outputTask.putFile(file).addOnCompleteListener(uploadTask -> {
            if(uploadTask.isSuccessful()) {
                outputTask.getDownloadUrl().addOnCompleteListener(task);
            }
        });
    }

    public void getUri(String targetPath, OnCompleteListener<Uri> task) {
        storageReference
                .child(targetPath)
                .getDownloadUrl()
                .addOnCompleteListener(task);
    }

    public static String getProfilePicUriPath(String userId) {
        return IMAGE_PATH + "/" + USER_PATH + "/" + userId + "/" + PROFILE_PIC_NAME;
    }

    public static String getProjectMainPicPath(String userId, String projectName) {
        return IMAGE_PATH + "/" + USER_PATH + "/" + userId + "/" + PROJECTS_PATH + "/" + projectName + ".jpg";
    }
}
