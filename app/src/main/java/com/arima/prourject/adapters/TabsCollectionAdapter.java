package com.arima.prourject.adapters;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.arima.prourject.fragments.CreateProjectFragment;
import com.arima.prourject.fragments.DemoObjectFragment;

public class TabsCollectionAdapter extends FragmentStateAdapter {
    public TabsCollectionAdapter(Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
            case 1:
            case 2:
                fragment = new DemoObjectFragment();
                break;
            case 3:
                fragment = new CreateProjectFragment();
        }
        Bundle args = new Bundle();
        // Our object is just an integer :-P
        args.putInt(DemoObjectFragment.ARG_OBJECT, position + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}