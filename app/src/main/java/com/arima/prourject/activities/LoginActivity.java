package com.arima.prourject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.arima.prourject.R;
import com.arima.prourject.managers.CacheManager;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.managers.SharedPreferencesManager;
import com.arima.prourject.model.User;
import com.arima.prourject.model.UserProfile;
import com.arima.prourject.util.ActivityExtra;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends DefaultActivity {
    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setLayout() {
        setContentView(R.layout.activity_login);
    }

    @Override
    public void createViews() {
        super.createViews();
        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);
    }

    public void login(View view) {
        if(!validate()) return;

        setLoadingState(true);

        authManager.login(thisActivity, email.getText().toString(), password.getText().toString(), task -> {
            if(!task.isSuccessful() || !authManager.isLogin()) {
                authManager.logout();
                Snackbar.make(view, R.string.login_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }
            if(authManager.isLogin()) {
                loadUserProfile(authManager.getUser());
            }
        });
    }

    public void create(View view) {
        ActivityExtra.redirectTo(thisActivity, CreateAccountActivity.class, false);
    }

    public void recover(View view) {
        ActivityExtra.redirectTo(thisActivity, RecoverPasswordActivity.class, false);
    }

    public boolean validate() {
        boolean valid = true;
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        if(emailText.isEmpty()) {
            valid = false;
            email.setError(getString(R.string.login_error_no_email));
        }
        if(passwordText.isEmpty()) {
            valid = false;
            password.setError(getString(R.string.login_error_no_password));
        }
        return valid;
    }

    public void showPassword(View view) {
        showPasswordLikeText(password);
    }

    public void showPasswordLikeText(EditText passwordLike) {
        if(passwordLike.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            passwordLike.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passwordLike.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else {
            passwordLike.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passwordLike.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        passwordLike.setSelection(passwordLike.getText().length());
    }

    private void loadUserProfile(User user) {
        firestoreManager.getDocument(
                FirestoreManager.FS_COLLECTION_USERS,
                user.getId(), response -> {
                    UserProfile userProfile = new UserProfile();
                    userProfile.setName(getString(R.string.profile_user_name));
                    userProfile.setMessage(getString(R.string.profile_user_message));
                    userProfile.setProfilePictureVersion(null);

                    if(!response.isSuccessful()) {
                        preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
                        ActivityExtra.redirectTo(thisActivity, MainActivity.class, true);
                        return;
                    }

                    UserProfile externalUserProfile = response.getResult().toObject(UserProfile.class);

                    if(externalUserProfile == null) {
                        preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
                        ActivityExtra.redirectTo(thisActivity, MainActivity.class, true);
                        return;
                    }

                    preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, externalUserProfile);
                    ActivityExtra.redirectTo(thisActivity, MainActivity.class, true);
                });
    }
}
