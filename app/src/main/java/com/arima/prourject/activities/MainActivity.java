package com.arima.prourject.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.arima.prourject.R;
import com.arima.prourject.managers.FirebaseStorageManager;
import com.arima.prourject.managers.SharedPreferencesManager;
import com.arima.prourject.model.UserProfile;
import com.arima.prourject.util.ActivityExtra;

import android.view.MenuItem;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Menu;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends DefaultActivity {
    //private FloatingActionButton filterProjectsButton;
    private AppBarConfiguration mAppBarConfiguration;

    private UserProfile userProfile;

    private CircleImageView picture;
    private TextView name;
    private TextView email;

    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_projects, R.id.nav_share)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    protected void onResume() {
        super.onResume();
        validateLogin();
    }

    @Override
    void setLayout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void createViews() {
        super.createViews();
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        //filterProjectsButton = findViewById(R.id.filter_projects_button);
        //filterProjectsButton.setOnClickListener(view -> filterProjects(filterProjectsButton));

        View headerView = navigationView.getHeaderView(0);
        picture = headerView.findViewById(R.id.nav_header_picture);
        email = headerView.findViewById(R.id.nav_header_email);
        name = headerView.findViewById(R.id.nav_header_name);
    }

    //public void filterProjects(View view) {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void validateLogin() {
        if(!authManager.isLogin()) {
            logout();
        }
        authManager.reload(thisActivity, task -> {
            if(!task.isSuccessful() || !authManager.isLogin()) {
                logout();
            } else {
                refreshUserProfile();
            }
        });
    }

    public void refreshUserProfile() {
        userProfile = (UserProfile) preferencesManager.getObjectPreference(SharedPreferencesManager.SP_USER_PROFILE, UserProfile.class);
        String userProfilePicLocalVersion = preferencesManager.getStringPreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V);

        email.setText(authManager.getUser().getEmail());
        name.setText(userProfile.getName() == null ? getString(R.string.profile_user_name) : userProfile.getName());
        Drawable image = cacheManager.readImageFromCache(FirebaseStorageManager.PROFILE_PIC_NAME);
        if(image != null && userProfile.getProfilePictureVersion().equals(userProfilePicLocalVersion)) {
            picture.setImageDrawable(image);
        } else if(userProfile.getProfilePictureVersion() != null){
            storageManager.getUri(
                    FirebaseStorageManager.getProfilePicUriPath(authManager.getUser().getId()),
                    uriTask -> {
                        if(!uriTask.isSuccessful()) {
                            return;
                        }
                        Picasso.get().load(uriTask.getResult()).into(picture, new Callback() {
                            @Override
                            public void onSuccess() {
                                preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V, userProfile.getProfilePictureVersion());
                                Bitmap newBitmap = ((BitmapDrawable)picture.getDrawable()).getBitmap();
                                cacheManager.saveImageInCache(newBitmap, FirebaseStorageManager.PROFILE_PIC_NAME, 70);
                            }

                            @Override
                            public void onError(Exception e) {}
                        });
                    });
        }
    }

    public void logout() {
        authManager.logout();
        preferencesManager.clearSharedPreferences();
        ActivityExtra.redirectTo(thisActivity, LoginActivity.class, true);
    }

    public void goToProfile(View view) {
        ActivityExtra.redirectTo(thisActivity, ProfileActivity.class, false);
    }
}
