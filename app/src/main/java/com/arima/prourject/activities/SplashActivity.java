package com.arima.prourject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.arima.prourject.R;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.managers.SharedPreferencesManager;
import com.arima.prourject.model.User;
import com.arima.prourject.model.UserProfile;

public class SplashActivity extends DefaultActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkUserAndRedirect();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void setLayout() {}

    @Override
    public void setDefaultValues() {
        withActionBar = false;
    }

    private void checkUserAndRedirect() {
        if(!authManager.isLogin()) {
            invalidLogin(false);
            return;
        }
        User currentUser = authManager.getUser();
        preferencesManager.savePreference(SharedPreferencesManager.SP_USER, currentUser);

        // Load your default values here
        loadUserProfile(currentUser);
    }

    private void invalidLogin(boolean logout) {
        if(logout) {
            authManager.logout();
        }
        preferencesManager.clearSharedPreferences();
        redirectToLogin();
    }

    private void redirectToLogin() {
        Intent intent = new Intent(thisActivity, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void redirectToMain() {
        Intent intent = new Intent(thisActivity, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void loadUserProfile(User user) {
        UserProfile savedProfile = (UserProfile) preferencesManager.getObjectPreference(SharedPreferencesManager.SP_USER_PROFILE, UserProfile.class);
        if(savedProfile == null) {
            preferencesManager.removePreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V);
        }

        firestoreManager.getDocument(
                FirestoreManager.FS_COLLECTION_USERS,
                user.getId(), response -> {
                    UserProfile userProfile = new UserProfile();
                    userProfile.setName(getString(R.string.profile_user_name));
                    userProfile.setMessage(getString(R.string.profile_user_message));
                    userProfile.setProfilePictureVersion(null);

                    if(!response.isSuccessful()) {
                        if(savedProfile == null) {
                            preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
                        }
                        redirectToMain();
                        return;
                    }

                    UserProfile externalUserProfile = response.getResult().toObject(UserProfile.class);

                    if(externalUserProfile == null) {
                        if(savedProfile == null) {
                            preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
                        }
                        redirectToMain();
                        return;
                    }

                    preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, externalUserProfile);
                    redirectToMain();
                });
    }
}
