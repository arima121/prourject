package com.arima.prourject.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.arima.prourject.R;
import com.arima.prourject.managers.FirebaseStorageManager;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.managers.SharedPreferencesManager;
import com.arima.prourject.model.UserProfile;
import com.arima.prourject.util.ImageExtras;
import com.arima.prourject.util.GsonExtras;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends DefaultActivity {
    private CardView cancelCardView;
    private CardView saveCardView;
    private CardView pictureCardView;
    private CardView editCardView;

    private CardView editProfile;
    private CardView showProfile;

    private EditText userNameEdit;
    private EditText userMessageEdit;

    private TextView userName;
    private TextView userMessage;

    private ImageButton saveButton;

    private CircleImageView profilePicture;

    private Bitmap selectedProfileImage;

    private PopupWindow popup;

    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        loadUserProfile();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setLayout() {
        setContentView(R.layout.activity_profile);
    }

    @Override
    public void createViews() {
        super.createViews();
        cancelCardView = findViewById(R.id.card_cancel);
        saveCardView = findViewById(R.id.card_save);
        pictureCardView = findViewById(R.id.card_picture);
        editCardView = findViewById(R.id.card_edit);
        showProfile = findViewById(R.id.regular_view);
        editProfile = findViewById(R.id.edit_view);

        userNameEdit = findViewById(R.id.user_name_edit);
        userMessageEdit = findViewById(R.id.user_message_edit);
        userName = findViewById(R.id.user_name);
        userMessage = findViewById(R.id.user_message);

        saveButton = findViewById(R.id.save);

        profilePicture = findViewById(R.id.profile_picture);
    }

    public void edit(View view) {
        cancelCardView.setVisibility(View.VISIBLE);
        saveCardView.setVisibility(View.VISIBLE);
        pictureCardView.setVisibility(View.VISIBLE);
        editCardView.setVisibility(View.GONE);
        showProfile.setVisibility(View.GONE);
        editProfile.setVisibility(View.VISIBLE);
    }

    public void cancel(View view) {
        cancelCardView.setVisibility(View.GONE);
        saveCardView.setVisibility(View.GONE);
        pictureCardView.setVisibility(View.GONE);
        editCardView.setVisibility(View.VISIBLE);
        editProfile.setVisibility(View.GONE);
        showProfile.setVisibility(View.VISIBLE);

        userMessageEdit.setText(userMessage.getText());
        userNameEdit.setText(userName.getText());
    }

    public void save(View view) {
        Map<String, Object> userProfile = createProfileData();
        setLoadingState(true);
        firestoreManager.saveObject(
                FirestoreManager.FS_COLLECTION_USERS,
                authManager.getUser().getId(),
                GsonExtras.noNUllMap(userProfile),
                task -> {
                    if(!task.isSuccessful()) {
                        setLoadingState(false);
                        Snackbar.make(saveButton, getString(R.string.profile_error_save), Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    UserProfile newUserProfile = (UserProfile) preferencesManager.getObjectPreference(SharedPreferencesManager.SP_USER_PROFILE, UserProfile.class);
                    newUserProfile.setName((String)userProfile.get(UserProfile.FS_FIELD_NAME));
                    newUserProfile.setMessage((String)userProfile.get(UserProfile.FS_FIELD_MESSAGE));
                    this.userProfile = newUserProfile;
                    preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, newUserProfile);

                    cancelCardView.setVisibility(View.GONE);
                    saveCardView.setVisibility(View.GONE);
                    pictureCardView.setVisibility(View.GONE);
                    editCardView.setVisibility(View.VISIBLE);
                    editProfile.setVisibility(View.GONE);
                    showProfile.setVisibility(View.VISIBLE);

                    userMessageEdit.setText(newUserProfile.getMessage());
                    userNameEdit.setText(newUserProfile.getName());

                    userMessage.setText(newUserProfile.getMessage());
                    userName.setText(newUserProfile.getName());
                    setLoadingState(false);
                });
    }

    public void loadUserProfile() {
        setLoadingState(true);
        UserProfile userProfileSaved = (UserProfile) preferencesManager.getObjectPreference(SharedPreferencesManager.SP_USER_PROFILE, UserProfile.class);
        firestoreManager.getDocument(
                FirestoreManager.FS_COLLECTION_USERS,
                authManager.getUser().getId(),
                documentSnapshot -> {
                    if(!documentSnapshot.isSuccessful()) {
                        loadUserProfileData(userProfileSaved);
                        setLoadingState(false);
                        return;
                    }
                    userProfile = documentSnapshot.getResult().toObject(UserProfile.class);
                    if(userProfile == null) {
                        loadUserProfileData(userProfileSaved);
                        setLoadingState(false);
                        return;
                    }
                    loadUserProfileData(userProfile);
                    setLoadingState(false);
                });
    }

    public void loadUserProfileData(UserProfile userProfile) {
        String message = (userProfile.getMessage() == null) ? getString(R.string.profile_user_message) : userProfile.getMessage();
        String name = (userProfile.getName() == null) ? getString(R.string.profile_user_name) : userProfile.getName();
        userMessage.setText(message);
        userName.setText(name);

        userMessageEdit.setText(message);
        userNameEdit.setText(name);

        String profilePicVersionSaved = preferencesManager.getStringPreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V);
        if(userProfile.getProfilePictureVersion().equals(profilePicVersionSaved)) {
            Drawable image = cacheManager.readImageFromCache(FirebaseStorageManager.PROFILE_PIC_NAME);
            if(image != null) {
                profilePicture.setImageDrawable(image);
            }
        } else {
            preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
            storageManager.getUri(
                    FirebaseStorageManager.getProfilePicUriPath(authManager.getUser().getId()),
                    uriTask -> {
                        if(!uriTask.isSuccessful()) {
                            return;
                        }
                        Picasso.get().load(uriTask.getResult()).into(profilePicture, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V, userProfile.getProfilePictureVersion());
                                Bitmap newBitmap = ((BitmapDrawable)profilePicture.getDrawable()).getBitmap();
                                cacheManager.saveImageInCache(newBitmap, FirebaseStorageManager.PROFILE_PIC_NAME, 70);
                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });
                    });
        }
    }

    public Map<String, Object> createProfileData() {
        Map<String, Object> userProfile = new HashMap<>();

        userProfile.put(UserProfile.FS_FIELD_MESSAGE, userMessageEdit.getText().toString());
        userProfile.put(UserProfile.FS_FIELD_NAME, userNameEdit.getText().toString());

        return userProfile;
    }

    public void displayPopupWindow(View anchorView) {
        popup = new PopupWindow(thisActivity);
        View layout = getLayoutInflater().inflate(R.layout.pop_up_camera_menu, null);
        popup.setContentView(layout);

        popup.setOutsideTouchable(true);
        popup.setFocusable(true);

        popup.setBackgroundDrawable(null);
        popup.showAsDropDown(anchorView);
    }

    public void takePicture(View view) {
        popup.dismiss();
        Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);
    }

    public void openGallery(View view) {
        popup.dismiss();
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_CANCELED) {
            Bitmap old = ((BitmapDrawable)profilePicture.getDrawable()).getBitmap();
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        selectedProfileImage = (Bitmap) data.getExtras().get("data");
                        selectedProfileImage = ImageExtras.scaleDown(selectedProfileImage, 320, true);
                        profilePicture.setImageBitmap(selectedProfileImage);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri photoUri = data.getData();
                        if (photoUri != null) {
                            try {
                                selectedProfileImage = MediaStore.Images.Media.getBitmap(thisActivity.getContentResolver(), photoUri);
                                selectedProfileImage = ImageExtras.scaleDown(selectedProfileImage, 320, true);
                                profilePicture.setImageBitmap(selectedProfileImage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
            builder.setPositiveButton(R.string.profile_accept_image, (dialog, id) -> {
                setLoadingState(true);

                String newProfilePicVersion = UUID.randomUUID().toString();
                cacheManager.saveImageInCache(selectedProfileImage, FirebaseStorageManager.PROFILE_PIC_NAME, 70);
                preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE_PIC_LOCAL_V, newProfilePicVersion);
                UserProfile userProfile = (UserProfile) preferencesManager.getObjectPreference(SharedPreferencesManager.SP_USER_PROFILE, UserProfile.class);
                userProfile.setProfilePictureVersion(newProfilePicVersion);
                preferencesManager.savePreference(SharedPreferencesManager.SP_USER_PROFILE, userProfile);
                Map<String, Object> fields = new HashMap<>();
                fields.put(UserProfile.FS_FIELD_PROFILE_PICTURE_VERSION, newProfilePicVersion);
                firestoreManager.saveObject(
                        FirestoreManager.FS_COLLECTION_USERS,
                        authManager.getUser().getId(),
                        fields,
                        userSaved -> {
                            if(!userSaved.isSuccessful()) {
                                setLoadingState(false);
                                return;
                            }
                            storageManager.uploadFile(
                                    new File(cacheManager.getCachePath(), FirebaseStorageManager.PROFILE_PIC_NAME), FirebaseStorageManager.getProfilePicUriPath(authManager.getUser().getId()),
                                    uploadTask -> {
                                        if(!uploadTask.isSuccessful()) {
                                            setLoadingState(false);
                                            return;
                                        }
                                        setLoadingState(false);
                                    });
                        });
            });
            builder.setNegativeButton(R.string.profile_cancel_image, (dialog, id) -> {
                profilePicture.setImageBitmap(old);
                selectedProfileImage = null;
            });
            builder.setTitle(getString(R.string.profile_confirm_tittle_image));
            builder.setCancelable(false);
            AlertDialog dialog = builder.create();
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setDimAmount(0.1f);
            dialog.show();
        }
    }
}
