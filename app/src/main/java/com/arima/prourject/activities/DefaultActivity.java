package com.arima.prourject.activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.arima.prourject.R;
import com.arima.prourject.managers.CacheManager;
import com.arima.prourject.managers.FirebaseAuthManager;
import com.arima.prourject.managers.FirebaseStorageManager;
import com.arima.prourject.managers.FirestoreManager;
import com.arima.prourject.managers.SharedPreferencesManager;

public abstract class DefaultActivity extends AppCompatActivity {
    protected DefaultActivity thisActivity;

    protected FirebaseAuthManager authManager;
    protected FirestoreManager firestoreManager;
    protected SharedPreferencesManager preferencesManager;
    protected FirebaseStorageManager storageManager;
    protected CacheManager cacheManager;

    protected ProgressBar loadingPB;
    protected Toolbar toolbar;

    protected boolean withActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDefaultValues();
        setLayout();
        createInternalItems();
        createViews();
    }

    public void setDefaultValues() {
        withActionBar = true;
    }

    abstract void setLayout();

    public void createInternalItems() {
        thisActivity = this;

        authManager = new FirebaseAuthManager();
        firestoreManager = new FirestoreManager();
        storageManager = new FirebaseStorageManager();
        preferencesManager = new SharedPreferencesManager(thisActivity);
        cacheManager = new CacheManager(thisActivity);
    }

    public void createViews() {
        loadingPB = findViewById(R.id.loading);

        if(withActionBar) {
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setLoadingState(boolean loading) {
        if(loading) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

        loadingPB.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(loadingPB != null) {
            loadingPB.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}