package com.arima.prourject.model;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.PropertyName;

public class UserProject {
    private boolean active;
    private String name;
    private Timestamp creationDate;

    @PropertyName("active")
    public void setActive(boolean active) {
        this.active = active;
    }

    @PropertyName("active")
    public boolean getActive() {
        return active;
    }

    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("creation_date")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    @PropertyName("creation_date")
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}
