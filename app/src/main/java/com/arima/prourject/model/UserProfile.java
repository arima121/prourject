package com.arima.prourject.model;

import com.google.firebase.firestore.PropertyName;

public class UserProfile {
    private String name;
    private String message;
    private String profilePictureVersion;

    public static final String FS_FIELD_NAME = "name";
    public static final String FS_FIELD_MESSAGE = "message";
    public static final String FS_FIELD_PROFILE_PICTURE_VERSION = "profilePictureVersion";

    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("message")
    public String getMessage() {
        return message;
    }

    @PropertyName("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @PropertyName("profilePictureVersion")
    public String getProfilePictureVersion() {
        return profilePictureVersion;
    }

    @PropertyName("profilePictureVersion")
    public void setProfilePictureVersion(String profilePictureVersion) {
        this.profilePictureVersion = profilePictureVersion;
    }
}
